package br.com.app.domain.model;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ErrorResponseTest {

    @Test
    void testGetterAndSetter() {
        LocalDateTime timestamp = LocalDateTime.now();
        int status = 404;
        String path = "/api/users";
        String traceId = "123456789";
        String code = "RESOURCE_NOT_FOUND";
        String message = "The requested resource was not found.";
        String cause = "ResourceNotFoundException";

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(timestamp);
        errorResponse.setStatus(status);
        errorResponse.setPath(path);
        errorResponse.setTraceId(traceId);
        errorResponse.setCode(code);
        errorResponse.setMessage(message);
        errorResponse.setCause(cause);

        assertEquals(timestamp, errorResponse.getTimestamp());
        assertEquals(status, errorResponse.getStatus());
        assertEquals(path, errorResponse.getPath());
        assertEquals(traceId, errorResponse.getTraceId());
        assertEquals(code, errorResponse.getCode());
        assertEquals(message, errorResponse.getMessage());
        assertEquals(cause, errorResponse.getCause());
    }

    @Test
    void testToString() {
        LocalDateTime timestamp = LocalDateTime.now();
        int status = 404;
        String path = "/api/users";
        String traceId = "any_traceId";
        String code = "RESOURCE_NOT_FOUND";
        String message = "The requested resource was not found.";
        String cause = "ResourceNotFoundException";

        ErrorResponse errorResponse = ErrorResponse.builder()
                .timestamp(timestamp)
                .status(status)
                .path(path)
                .traceId(traceId)
                .code(code)
                .message(message)
                .cause(cause)
                .build();

        String expectedToString = "{"
                + "timestamp=" + timestamp
                + ", status=" + status
                + ", path=" + path
                + ", traceId=" + traceId
                + ", code=" + code
                + ", message=" + message
                + ", cause=" + cause
                + "}";

        assertEquals(expectedToString, errorResponse.toString());
    }

}