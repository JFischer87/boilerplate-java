# 🚀 Boilerplate

---
Este foi criado para ser um boilerplate para aplicação java com spring.

O código foi desenvolvido seguindo os princípios de Clean Architecture, DDD, TDD e as boas práticas.

- [Ferramentas necessárias](#ferr)
- [Padrões de projeto](#pdp)
- [Arquitetura](#arquitetura)
- [Piramide de teste](#ptestes)
- [Setup de Projeto](#setupprojeto)
- [Rodando o projeto](#rodandoprojeto)
- [Rodando os testes](#rodandotestes)
- [Qualidade e segurança](#qualidade)
- [Links uteis](#links)


## <a id = "ferr"></a> Ferramentas necessárias
- JDK 21 +
- IDE de sua preferência
  - IntelliJ IDEA 2023.3.4 ou superior
- Docker
- Plugins
  - Sonarlint <br> 
  ![](imgs/plugin-sonarlint.png)
  - ChechStyle <br> 
  ![](imgs/plugin-checkstyle.png)
  - SpotBugs <br> 
  ![](imgs/plugin-spotbugs.png)

##  <a id = "pdp"></a> Padrões de projeto
- Padrões Táticos DDD: https://vaadin.com/blog/ddd-part-2-tactical-domain-driven-design
- Arquitetura limpa: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

###  Padrões de branchs

### Padrões de commit
Utilizar padrões de commit ajuda a criar mensagens claras e concisas, facilitando a compreensão das alterações feitas 
no código. Isso é especialmente importante em projetos de desenvolvimento colaborativo, onde várias pessoas podem estar 
envolvidas e precisam entender o que cada commit representa.

Commits bem formatados e descritivos ajudam a construir um histórico de alterações mais legível e organizado. Isso 
permite que você e outros desenvolvedores naveguem pelo histórico do projeto, compreendam as mudanças realizadas e 
encontrem informações relevantes mais facilmente.

A mensagem de confirmação deve ser estruturada da seguinte forma:

``` text
<type>[optional scope]: <description>
```
Tipos utilizado no type:

- **feat** (Feature): Usado quando uma nova funcionalidade é adicionada ao projeto. Por exemplo, ao implementar um novo 
recurso ou adicionar uma nova página ao aplicativo.
<br><br>
- **fix** (Bug Fix): Utilizado quando um problema ou bug é corrigido no código. Isso pode incluir correções de erros, 
falhas de segurança ou comportamentos inesperados.
<br><br>
- **refactor** (Code Refactoring): Usado quando o código existente é reestruturado, melhorado ou otimizado, sem alterar 
seu comportamento externo. Isso pode incluir melhorias de desempenho, simplificação do código ou aplicação de boas 
práticas.
<br><br>
- **docs** (Documentation): Utilizado para alterações ou adições à documentação do projeto. Isso inclui atualizações em 
arquivos de documentação, como README, guias do usuário, comentários em código, etc.
<br><br>
- **test** (Testing): Usado quando são adicionados testes unitários, testes de integração ou qualquer tipo de teste 
automatizado para garantir a qualidade e integridade do código.
<br><br>
- **chore** (Chores): Utilizado para tarefas de manutenção geral do projeto, como atualização de dependências, 
configurações de build, tarefas de gerenciamento, entre outros.
<br><br>
- **style** (Code Styling): Usado para alterações relacionadas à formatação, estilo ou convenções de código. 
Isso pode incluir ajustes de indentação, espaçamento, nomeação de variáveis, entre outros.

``` shell
git commit -m docs(changelog): update changelog to version 0.0.1
```

## <a id = "arquitetura"></a> Arquitetura
Aggregates and Value Objects

![](imgs/tactical-explanation.png)

Structure Clean Architecture

![](imgs/ddd-architecture.png)

Big Picture Tactical Patterns DDD with Clean Architecture

![](imgs/Tactical Patterns DDD-Big Picture.jpg)

## <a id = "ptestes"></a> Piramide de teste

A Pirâmide de Testes é um conceito amplamente utilizado no desenvolvimento de software para orientar a estratégia de 
testes em um projeto. Ela representa a distribuição ideal dos diferentes tipos de testes em diferentes níveis de 
granularidade, formando uma pirâmide invertida.

A pirâmide é composta por três camadas principais:

**Testes Unitários:** <br>
Na base da pirâmide, temos os testes unitários. Eles são responsáveis por testar unidades individuais de código, 
como funções, métodos ou classes, de forma isolada. Os testes unitários garantem que cada componente funcione 
corretamente, verificando seu comportamento em relação a diferentes cenários e entradas. Esses testes são rápidos de 
serem executados e fornecem um feedback imediato, sendo essenciais para a detecção precoce de bugs e para manter a 
integridade do código.

**Testes de Integração:** <br>
No nível intermediário da pirâmide, encontram-se os testes de integração. Esses testes verificam a interação entre 
diferentes componentes do sistema, garantindo que eles se integrem corretamente e funcionem em conjunto. Isso envolve 
testar a comunicação entre módulos, a integração de serviços externos, bancos de dados e outras dependências. Os testes 
de integração são mais demorados e complexos do que os testes unitários, pois envolvem cenários mais amplos e cobrem 
fluxos de trabalho mais completos.

**Testes de Interface do Usuário:** <br>
No topo da pirâmide, temos os testes de interface do usuário. Esses testes, também conhecidos como testes end-to-end, 
simulam a interação do usuário com o sistema em condições reais. Eles validam a funcionalidade geral do aplicativo, 
testando diferentes fluxos de trabalho, telas e interações do usuário. Os testes de interface do usuário são mais 
lentos e complexos de serem configurados, pois podem envolver a automação de tarefas como preenchimento de formulários, 
cliques em botões e navegação em diferentes telas.


* Concept Martin Fowler : https://martinfowler.com/articles/practical-test-pyramid.html
  <br>

![](imgs/pyramid-tests.png)

## <a id = "setupprojeto"></a> Setup do Projeto
<br>
Usando o IntelliJ será necessário também a adicionar a opção VM Options para definir
o profile que será utilizado para a execução da aplicaçao.
<br>

```bash 
-Dspring.profiles.active=dev
```

### Configuração do maven no IntelliJ IDEA
Para que a IDE consiga baixar as dependências corretamente é necessário configuração das propriedades de build,
para isso precisamos acessar: 
``` text
Preferencia -> Build, Execution, Deployment -> Build Tooles -> Maven
```
E adicionar oo Maven home path `Bundled (Maven 3)`

![](imgs/config-maven-intellij.png)

### Configurar o ambiente no IntelliJ IDEA
1. Para realizar a configuração da aplicação e do profile que iremos rodar a aplicação acesse a opção
`Add Confituration`

![](imgs/config-add-1.png)

2. Adicione uma nova aplicação

![](imgs/config-add-2.png)

3. Configure as variáveis 

![](imgs/config-add-3.png)

4. Para configurar qual profile irá rodar a aplicação acesse e configure `-Dspring.profiles.active=local`, caso queira
rodar em profile diferente basta mudar o `local` para o ambiente desejado ex: `-Dspring.profiles.active=dev`.
``` text
Modify options -> Add VM option
```

![](imgs/config-add-4.png)

### Configuração do profile e ambiente

Caso o profile ativo seja de `hml` as configurações serão buscadas no config server não havendo necessidade de ter
um arquivo local para configurações.

Para rodar com um profile `local` criar um arquivo `application-local.yml` e adicionar as variáveis abaixo e configurar
as outras variáveis necessárias. 

``` yaml
# DEFINIÇÕES DA APLICAÇÃO
server:
  port: 8090

# REST
restapi:
  client:
    connectTimeout: 60000
    connectionRequestTimeout: 60000
    readTimeout: 60000

# WEBCLIENT
webclient:
  response:
    size: 2097152
  client:
    connectTimeout: 1000

# LOGGING
logging:
  level:
    root: INFO
    web: INFO
```

### Configuração do checkstyle
Para esta configuração iremos primeira configurar a IDE e posteriormente o plugin, utilizaremos o arquivo do
checkstyle.xml presente na pasta `./config`.

####Configuraração do checkstyle na Intellij IDEA

1. Acessar as configurações.
``` text
Preferences -> Editor -> Code Style -> Java
```
![](imgs/plugin-checkstyle-config-1.png)

2. Abrir as configurações
``` text
Configs (icone de engrenagem) -> Import Scheme -> Checkstyle configuration
```

![](imgs/plugin-checkstyle-config-2.png)

3. Localizar o arquivo `./config/checkstyle.xml`, após isso basta deixar o mesmo selecionado para utilizar.
<br>
<br>
####Configuração do plugin de checkstyle

1. Acessar as configurações

![](imgs/plugin-checkstyle-config-3.png)

2. Adicionar um arquivo de configuração
``` text
+ (icone mais) -> Browser
```

![](imgs/plugin-checkstyle-config-4.png)

3. Localizar o arquivo ` ./config/checkstyle.xml`, após isso basta deixar o mesmo selecionado para utilizar.
Feito a adição do arquivo de configuração basta selecionar a opção no plugin para executar a avaliação.

![](imgs/plugin-checkstyle-config-5.png)

## <a id = "rodandoprojeto"></a> Rodando o projeto
Podemos rodar a aplicação de tuas maneiras, através da própria IDE ou usando comandos maven.

###1. Rodando a aplicação no IntelliJ IDEA
Após realizar todas as configurações presentes na seção de [Setup do Projeto](#setup-projeto) basta clicar no botão
`Run Application`. <br>
Caso necessário é possível rodar via Debug clicando no `Debug Application`

![](imgs/run-project-intellij.png)

###2. Rodando a aplicação via terminal
Necessário ter o maven instalado e configurado no sistema operacional utilizado.

####2.1 Como instalar o Maven: <br>
 
**Mac**
```shell
brew install maven
```

**Linux**
```shell
>sudo apt-get -y install maven
````

**Windowns**
Seguir o passo a passo presente na [documentação](https://maven.apache.org/install.html) do
site.

>Com o Maven instalado e configurado executar o comando.
```shell
mvn spring-boot:run -Dspring.profiles.active=hml
```

***Caso não queira instalar o Maven em seu sistema operacional também é possivel rodar apardir do ItelliJ usando
a ferramente do maven presente na barra lateral direita.***

## <a id = "rodandotestes"></a> Rodando os testes
### 1. Rodando os testes no IntelliJ IDEA
Para rodarmos os testes através da IDE primeiro é necessário realizar as configurações conforme abaixo.

![](imgs/run-tests-intellij-1.png)

Após realizar as configurações basta executar através do botão `Run Application` ou `Run with Coverage`, este
último irá executar os testes e apresentar um relatório de cobertura para as classes.

![](imgs/run-tests-intellij-2.png)

Os testes também podem ser executados diretamente nas classes através do botão de `Run Test`

![](imgs/run-tests-intellij-3.png)

###2. Rodando os testes via terminal
Necessário ter o maven instalado e configurado no sistema operacional utilizado.
Para rodar os testes utilizando o Maven utilizar o comando:
```shell
mvn clean test
```
Os relatório dos testes e cobertura podem ser acessados dentro da pasta `./target/site/jacoco`.

## <a id = "qualidade"></a> Qualidade e segurança

Desenvolver uma API robusta, segura e de alta qualidade é fundamental para garantir o bom funcionamento de um sistema e 
proteger os dados sensíveis dos usuários. <br>
Nesse contexto, é essencial adotar boas práticas que abordem a qualidade do código e a segurança da aplicação. <br>
Este documento tem como objetivo fornecer uma visão geral de algumas dessas práticas e ferramentas que são utilizadas 
para atingir esses objetivos.

### Secret Detection (Detecção de Segredos)

A detecção de segredos é uma prática crucial para evitar o vazamento acidental de informações sensíveis. 
É importante garantir que nenhum segredo, como senhas, chaves de API ou tokens de autenticação, esteja exposto ou 
armazenado diretamente no código-fonte da API. <br>
**A implemetação ocorre através da esteira do gitlab onde é utilizado o template 
`Security/Secret-Detection.gitlab-ci.yml`**

### Static Application Security Testing (SAST - Teste de Segurança Estático de Aplicações)

O SAST é uma técnica que permite identificar vulnerabilidades de segurança no código-fonte da API. 
Ferramentas SAST analisam o código em busca de padrões conhecidos de vulnerabilidades e fornecem feedback sobre 
possíveis problemas de segurança. **Implementamos no projeto o spotbugs-sast com o plugin do findsecbugs-plugin e 
integramos ao SonarQube.**

### Dependency Check (Verificação de Dependências)

Dependências desatualizadas ou vulneráveis podem representar um risco para a segurança da API. Para garantirmos que a
aplicação não tenha vulnerabilidades **implementamos o plugin de dependency-check da OWASP** para ajudar a identificar 
e corrigir possíveis vulnerabilidades causadas por dependências desatualizadas ou inseguras.

### Dependency Tracker (Rastreamento de Dependências):

Manter um rastreamento das dependências utilizadas pela API é importante para garantir que todas elas sejam seguras 
e confiáveis. Ferramentas de rastreamento de dependências, como o OWASP Dependency-Track permitem monitorar e analisar 
as dependências utilizadas, identificando possíveis vulnerabilidades ou problemas de segurança.
**Para gerar os reports para o dependecy tracker adicionamos ao código o plugin cyclonedx-maven-plugin**


###Code Style (Estilo de Código):
Manter um estilo de código consistente e legível é essencial para facilitar a manutenção, colaboração e identificação 
de possíveis problemas no código-fonte da API. Adotar e aplicar um guia de estilo de código pode melhorar a qualidade 
do código e tornar o desenvolvimento mais eficiente.
**Criamos um codestyle baseado no Google Java Style Guide e através do arquivo de checkstyle.xml e do plugin
maven-checkstyle-plugin é possível manter o padrão no desenvolvimento**

###Config Server (Servidor de Configuração):
O uso de um servidor de configuração, como o Spring Cloud Config ou o HashiCorp Consul, é uma boa prática para
armazenar as configurações da API de forma centralizada. Isso evita a exposição acidental de configurações sensíveis no 
código-fonte e facilita a atualização e o gerenciamento das configurações da aplicação.
**Adotamos como servidor de configuração o Spring Cloud Config**, abaixo temos um desenho da arquitetura.

![](imgs/architect-config-cloud.png)

## <a id = "links"></a> Links uteis

- [Documentação externa]()
- [Dynatrace]()
- [Grafana]()
- [Graylog]()
- [Sonar]()
