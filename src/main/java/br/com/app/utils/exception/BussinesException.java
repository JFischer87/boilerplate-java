package br.com.app.utils.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BussinesException extends RuntimeException {
    private final HttpStatus status;
    private final String code;
    private final String apiVersion;
    private final String traceId;
    public BussinesException(final String message,
                             final HttpStatus status,
                             final String code,
                             final String apiVersion,
                             final String traceId) {
        super(message);
        this.code = code;
        this.status = status;
        this.apiVersion = apiVersion;
        this.traceId = traceId;
    }
}
