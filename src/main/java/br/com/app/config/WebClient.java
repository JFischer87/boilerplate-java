package br.com.app.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClient {

    @Value("${webclient.client.connectTimeout}")
    private int connectTimeout;

    @Value("${webclient.response.size}")
    private int webClientResponseSize;

    @Bean
    public org.springframework.web.reactive.function.client.WebClient constructWebClient() {
        final HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeout)
                .responseTimeout(Duration.ofMillis(connectTimeout))
                .doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(connectTimeout,
                        TimeUnit.MILLISECONDS)).addHandlerLast(new WriteTimeoutHandler(connectTimeout,
                        TimeUnit.MILLISECONDS)));

        final ExchangeStrategies strategies = ExchangeStrategies.builder()
                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(webClientResponseSize))
                .build();

        return org.springframework.web.reactive.function.client.WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .exchangeStrategies(strategies)
                .build();
    }
}