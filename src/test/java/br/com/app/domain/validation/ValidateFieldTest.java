package br.com.app.domain.validation;

import br.com.app.domain.model.InfoApi;
import br.com.app.utils.MessageUtils;
import br.com.app.utils.exception.BussinesException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ValidateFieldTest {
    private static final String FIELD_NAME = "fieldName";
    ;

    private ValidateField validateField;

    @Mock
    private MessageUtils messageUtils;

    @Mock
    private InfoApi infoApi;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testIsNull() {
        validateField = ValidateField.forField(messageUtils, infoApi, null, FIELD_NAME);
        when(messageUtils.get(any(), any(), any())).thenReturn("O campo [fieldName] é nulo");

        assertThrows(BussinesException.class, () -> validateField.isNotNull());
    }

    @Test
    void testIsEmpty() {
        validateField = ValidateField.forField(messageUtils, infoApi, "", FIELD_NAME);
        when(messageUtils.get(any(), any(), any())).thenReturn("O campo [fieldName] está vazio");

        assertThrows(BussinesException.class, () -> validateField.isNotEmpty());
    }

    @Test
    void testSanitize() {
        String input = "  Any-String  ";
        String expectedOutput = "any-string";
        validateField = ValidateField.forField(messageUtils, new InfoApi(), input, FIELD_NAME);
        String actualOutput = validateField.sanetize();
        assertEquals(expectedOutput, actualOutput);
    }
}