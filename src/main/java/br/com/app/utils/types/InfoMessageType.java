package br.com.app.utils.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InfoMessageType {

    START_REQUEST("start.request", "00BxAPI00I"),
    SUCCESS(Constants.SUCCESS, "00BxAPI01I"),
    CREATED(Constants.SUCCESS, "00BxAPI02I"),
    NO_CONTENT(Constants.SUCCESS, "00BxAPI03I"),
    NO_MESSAGE_FOUND("no.message.found", "00BxAPI04E");

    private final String message;
    private final String code;
}
