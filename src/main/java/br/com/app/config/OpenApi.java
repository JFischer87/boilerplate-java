package br.com.app.config;


import br.com.app.domain.model.InfoApi;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class OpenApi {

    @Autowired
    private InfoApi infoApi;

    @Bean
    public GroupedOpenApi api() {
        return GroupedOpenApi.builder()
                .group(infoApi.getCapability())
                .pathsToMatch("/**")
                .build();
    }

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .components(new Components())
                .externalDocs(new ExternalDocumentation()
                        .description("Documentação")
                        .url(infoApi.getLinkDoc()))
                .info(new Info()
                        .title(infoApi.getName())
                        .description(infoApi.getDescription())
                        .contact(new Contact()
                                .name(infoApi.getContactName())
                                .email(infoApi.getContactEmail()))
                        .version("v" + infoApi.getVersion())
                );
    }
}