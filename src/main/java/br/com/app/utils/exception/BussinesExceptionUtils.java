package br.com.app.utils.exception;

import br.com.app.domain.model.InfoApi;
import br.com.app.utils.MessageUtils;
import br.com.app.utils.types.ErrorMessageType;
import org.springframework.stereotype.Component;

@Component
public class BussinesExceptionUtils {

    private BussinesExceptionUtils() {
        /*
        Sem necessidade neste momento
        */
    }

    public static void throwsBussinesExceptions(
            MessageUtils messageUtils,
            InfoApi infoApi,
            ErrorMessageType errorMessageType,
            Object... args){

        String message = messageUtils.get(errorMessageType.getMessage(), args);

        throw new BussinesException(
                message,
                errorMessageType.getHttpStatus(),
                errorMessageType.getCode(),
                infoApi.getVersion(),
                null
        );
    }
}
