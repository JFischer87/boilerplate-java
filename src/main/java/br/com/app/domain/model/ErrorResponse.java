package br.com.app.domain.model;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "ErrorResponse")
public class ErrorResponse {
    private LocalDateTime timestamp;
    private int status;
    private String path;
    private String traceId;
    private String code;
    private String message;
    private String cause;

    public String toString() {
        return "{"
                + "timestamp=" + timestamp
                + ", status=" + status
                + ", path=" + path
                + ", traceId=" + traceId
                + ", code=" + code
                + ", message=" + message
                + ", cause=" + cause
                + '}';
    }
}
