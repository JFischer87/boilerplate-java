package br.com.app.domain.validation;

import br.com.app.domain.model.InfoApi;
import br.com.app.utils.MessageUtils;
import br.com.app.utils.exception.BussinesExceptionUtils;
import br.com.app.utils.types.ErrorMessageType;

public final class ValidateField {

    private final InfoApi infoApi;
    private final MessageUtils messageUtils;
    private final String field;
    private final String fieldName;

    private ValidateField(final MessageUtils messageUtils, final InfoApi infoApi,
                          final String field, final String fieldName) {
        this.infoApi = InfoApi.getNewInfoApi(infoApi);
        this.messageUtils = messageUtils;
        this.field = field;
        this.fieldName = fieldName;
    }

    public static ValidateField forField(final MessageUtils messageUtils, final InfoApi infoApi,
                                         final String field, final String fieldName) {
        return new ValidateField(messageUtils, infoApi, field, fieldName);
    }

    public ValidateField isNotNull() {
        if (field == null) {
            BussinesExceptionUtils.throwsBussinesExceptions(messageUtils, infoApi,
                    ErrorMessageType.NULL_FIELD, fieldName);
        }
        return this;
    }

    public ValidateField isNotEmpty() {
        if (field.trim().isEmpty()) {
            BussinesExceptionUtils.throwsBussinesExceptions(messageUtils, infoApi,
                    ErrorMessageType.EMPTY_FIELD, fieldName);
        }
        return this;
    }
    public String sanetize() {
        return field.trim().toLowerCase();
    }

}
