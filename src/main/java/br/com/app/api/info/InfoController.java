package br.com.app.api.info;

import br.com.app.domain.model.ErrorResponse;
import br.com.app.domain.model.InfoApi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping(value = "/infos")
@Tag(name = "Info")
public class InfoController {

    private final InfoApi infoApi;
    public InfoController(InfoApi infoApi) {
        this.infoApi = infoApi;
    }
    @Operation(summary = "Traz as informações da aplicação")
    @ApiResponses(value = {
            @ApiResponse(responseCode  = "200", description = "Informações retornadas com sucesso",
                    content = {
                            @Content(schema =
                            @Schema(implementation = InfoApi.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Sistema Indisponivel",
                    content = {
                            @Content(schema =
                            @Schema(implementation = ErrorResponse.class), mediaType = "application/json")}),
    })
    @GetMapping()
    public ResponseEntity<InfoApi> getInfo() {
        return ResponseEntity.ok(infoApi);
    }
}
