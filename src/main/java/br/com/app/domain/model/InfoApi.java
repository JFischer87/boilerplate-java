package br.com.app.domain.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "app")
public class InfoApi {
    private String id;
    private String name;
    private String version;
    private String capability;
    private String description;
    private String contactName;
    private String contactEmail;
    private String linkDoc;

    public static InfoApi getNewInfoApi(final InfoApi infoApi) {
        return infoApi;
    }
}

