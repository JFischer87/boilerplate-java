package br.com.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {
    @Value("${restapi.client.connectTimeout}")
    private int connectTimeout;

    @Value("${restapi.client.readTimeout}")
    private int readTimeout;

    @Bean
    public RestTemplate restTemplate() {
        final SimpleClientHttpRequestFactory httpRequestFactory = new SimpleClientHttpRequestFactory();
        httpRequestFactory.setConnectTimeout(connectTimeout);
        httpRequestFactory.setReadTimeout(connectTimeout);

        return new RestTemplate(httpRequestFactory);
    }
}
