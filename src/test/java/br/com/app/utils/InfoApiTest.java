package br.com.app.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import br.com.app.domain.model.InfoApi;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = InfoApi.class)
class InfoApiTest {

    @Test
    void testGettersAndSetters() {
        InfoApi infoApi = new InfoApi();

        infoApi.setId("123");
        Assertions.assertEquals("123", infoApi.getId());

        infoApi.setName("Test");
        Assertions.assertEquals("Test", infoApi.getName());

        infoApi.setVersion("1.0");
        Assertions.assertEquals("1.0", infoApi.getVersion());

        infoApi.setCapability("Capability");
        Assertions.assertEquals("Capability", infoApi.getCapability());

        infoApi.setDescription("Description");
        Assertions.assertEquals("Description", infoApi.getDescription());

        infoApi.setContactName("any_name");
        Assertions.assertEquals("any_name", infoApi.getContactName());

        infoApi.setContactEmail("any-email@mail.com");
        Assertions.assertEquals("any-email@mail.com", infoApi.getContactEmail());
    }
}