
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
---
## [Released]
## [0.0.1] - 2023-07-02
[Jéferson Fischer] **feat/initial-project-setup** <br>
### Added
- feat: adicionado o arquivo de config do maven.
- test: criado a controller de info da api, config openapi e webclientconfig
- feat: criado a controller de info da api, config openapi e webclientconfig
- test: criado a classe da aplicação e os arquivos de configuração
- feat: criado a classe da aplicação e os arquivos de configuração
- feat: adicionadas as configurações do checkstyle, spotbug, lombok e logback
- test: adicionado o log handler interceptor e as classe de mensagens.
- feat: adicionado o log handler interceptor e as classe de mensagens.
- docs: atualizado o README.md
- docs: atualizado o Changelog.md
### Changed
### Removed
