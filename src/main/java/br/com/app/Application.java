package br.com.app;

import br.com.app.utils.MessageUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@ComponentScan(value = "br.com.app")
@Import(MessageUtils.class)
public class Application {
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
