package br.com.app.utils.types;

public final class Constants {

    private Constants() {
    }

    public static final String DEBUG = "debug";
    public static final String INFO = "info";
    public static final String WARN = "warn";
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
}


