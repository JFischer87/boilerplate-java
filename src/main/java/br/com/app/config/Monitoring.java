package br.com.app.config;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.logging.LogbackMetrics;
import io.micrometer.core.instrument.binder.system.DiskSpaceMetrics;
import io.micrometer.core.instrument.binder.system.FileDescriptorMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.core.instrument.config.MeterFilter;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Monitoring {

    private static final String UNKNOW = "unknow";
    private static final String DISK_SPACE_DIRECTORY = "/";
    private static final List<String> NON_APPLICATION_ENDPOINTS =
            Arrays.asList("/swagger", "/**", "/v3/api-docs",
                    "/webjars");
    private static final Logger LOGGER = Logger.getLogger(Monitoring.class.getName());
    private static final String TAG_URI = "uri";

    @Bean
    public static MeterRegistryCustomizer<MeterRegistry> metricsCommonTags(
            @Value("${spring.application.name}") final String applicationName) {
        return registry -> registry.config()
                .commonTags("host", getHostName(), "ip",
                        getHostAddress(), "application", applicationName)
                .meterFilter(denyFrameworkUrisFilter());
    }

    private static MeterFilter denyFrameworkUrisFilter() {
        return MeterFilter.deny(id -> isNonApplicationEndpoint(id.getTag(TAG_URI)));
    }

    private static boolean isNonApplicationEndpoint(final String uri) {
        return uri != null
                && NON_APPLICATION_ENDPOINTS.stream().map(uri::startsWith).filter(i -> i)
                .findFirst().orElse(false);
    }

    private static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            return UNKNOW;
        }
    }

    private static String getHostAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            return UNKNOW;
        }
    }

    @Bean
    UptimeMetrics uptimeMetrics() {
        return new UptimeMetrics();
    }

    @Bean
    ProcessorMetrics processorMetrics() {
        return new ProcessorMetrics();
    }

    @Bean
    FileDescriptorMetrics fileDescriptorMetrics() {
        return new FileDescriptorMetrics();
    }

    @Bean
    JvmMemoryMetrics jvmMemoryMetrics() {
        return new JvmMemoryMetrics();
    }

    @Bean
    JvmThreadMetrics jvmThreadMetrics() {
        return new JvmThreadMetrics();
    }

    @Bean
    JvmGcMetrics jvmGcMetrics() {
        return new JvmGcMetrics();
    }

    @Bean
    DiskSpaceMetrics diskSpaceMetrics() {
        return new DiskSpaceMetrics(new File(DISK_SPACE_DIRECTORY));
    }

    @Bean
    LogbackMetrics logbackMetrics() {
        return new LogbackMetrics();
    }
}