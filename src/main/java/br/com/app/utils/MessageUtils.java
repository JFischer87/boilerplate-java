package br.com.app.utils;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessageUtils {

    private static final String LOCALE = "locale";
    private static final String DEFAULT_LOCALE = "pt-BR";

    private MessageSource messageSource;

    public String get(final String messageType, final Object... args) {
        String[] arrLocale = DEFAULT_LOCALE.split("-");
        Locale messageLocale = Locale.of(arrLocale[0], arrLocale[1]);
        return messageSource.getMessage(messageType, args, messageLocale);
    }

    public static String getLocale(final HttpServletRequest request) {
        String locale = request.getParameter(LOCALE);
        if (locale == null || locale.isEmpty()) {
            request.setAttribute(LOCALE, DEFAULT_LOCALE);
            locale = Objects.requireNonNull(request.getAttribute(LOCALE)).toString();
        }
        return locale;
    }
}
