package br.com.app.config;

import io.micrometer.core.instrument.MeterRegistry;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class MonitoringTest {

    @Mock
    private MeterRegistry meterRegistry;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testUptimeMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.uptimeMetrics());
    }

    @Test
    void testProcessorMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.processorMetrics());
    }

    @Test
    void testFileDescriptorMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.fileDescriptorMetrics());
    }

    @Test
    void testJvmMemoryMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.jvmMemoryMetrics());
    }

    @Test
    void testJvmThreadMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.jvmThreadMetrics());
    }

    @Test
    void testJvmGcMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.jvmGcMetrics());
    }

    @Test
    void testDiskSpaceMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.diskSpaceMetrics());
    }

    @Test
    void testLogbackMetrics() {
        Monitoring config = new Monitoring();

        assertNotNull(config.logbackMetrics());
    }
}