package br.com.app.config;

import io.netty.handler.timeout.ReadTimeoutException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = WebClient.class)
@TestPropertySource(properties = {"webclient.client.connectTimeout=1000", "webclient.response.size=2097152"})
class WebClientTest {
    @Autowired
    private org.springframework.web.reactive.function.client.WebClient client;

    @Autowired
    private static MockWebServer mock;

    @BeforeAll
    static void setUp() throws IOException {
        mock = new MockWebServer();
        mock.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mock.shutdown();
    }

    @Test
    void testWebClientInstantiation() {
        Assertions.assertNotNull(client);
    }

    @Test
    void testWebClientReadTimeout() {
        mock.enqueue(new MockResponse().setBodyDelay(5000, TimeUnit.MILLISECONDS).setBody("{}"));

        String urlServer = String.format("http://localhost:%s", mock.getPort());
        Mono<String> bodyToMono = client.get().uri(urlServer).retrieve().bodyToMono(String.class);

        WebClientResponseException exception = assertThrows(WebClientResponseException.class, bodyToMono::block);
        Assertions.assertTrue(exception.getCause() instanceof ReadTimeoutException);
    }
}