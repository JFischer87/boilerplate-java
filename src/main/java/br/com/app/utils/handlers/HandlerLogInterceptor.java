package br.com.app.utils.handlers;

import br.com.app.utils.MessageUtils;
import br.com.app.utils.types.InfoMessageType;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Component
public class HandlerLogInterceptor implements HandlerInterceptor {
    private static final String TRACE_ID = "x-trace-id";
    private final MessageUtils messageUtils;
    public HandlerLogInterceptor(final MessageUtils messageUtils) {
        this.messageUtils = messageUtils;
    }

    @Override
    public boolean preHandle(@NotNull final HttpServletRequest request,
                             @NotNull final HttpServletResponse response,
                             @NotNull final Object handler) {

        final String path = (String) request.getAttribute("org.springframework.web.servlet.HandlerMapping"
                + ".pathWithinHandlerMapping");

        final String traceId = request.getHeader(TRACE_ID);

        log.info(messageUtils.get(InfoMessageType.START_REQUEST.getMessage(),
                path, traceId));
        return true;
    }

    @Override
    public void postHandle(@NotNull final HttpServletRequest request,
                           @NotNull final HttpServletResponse response,
                           @NotNull final Object handler,
                           final ModelAndView modelAndView) {

        final String path = (String) request.getAttribute("org.springframework.web.servlet.HandlerMapping"
                + ".pathWithinHandlerMapping");

        final String traceId = request.getHeader(TRACE_ID);

        log.info(messageUtils.get(InfoMessageType.SUCCESS.getMessage(), path, traceId));
    }

    @Override
    public void afterCompletion(@NotNull final HttpServletRequest request,
                                @NotNull final HttpServletResponse response,
                                @NotNull final Object handler,
                                final Exception ex) {
    }
}
