package br.com.app.utils.types;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorMessageType {
    NULL_FIELD("field.null.error", "00BxAPI01E", HttpStatus.BAD_REQUEST),
    EMPTY_FIELD("field.empty.error", "00BxAPI02E", HttpStatus.BAD_REQUEST),
    BODY_MISSING("body.missing", "00BxAPI03E", HttpStatus.BAD_REQUEST);

    private final String message;
    private final String code;
    private final HttpStatus httpStatus;
}
