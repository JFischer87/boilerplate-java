package br.com.app.config;

import br.com.app.utils.handlers.HandlerLogInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RequiredArgsConstructor
@Component
public class LogInterceptor implements WebMvcConfigurer {

    private final HandlerLogInterceptor inperceptor;

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(inperceptor);
    }
}
