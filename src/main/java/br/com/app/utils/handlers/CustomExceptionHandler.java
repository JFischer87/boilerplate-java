package br.com.app.utils.handlers;

import br.com.app.domain.model.ErrorResponse;
import br.com.app.domain.model.InfoApi;
import br.com.app.utils.MessageUtils;
import br.com.app.utils.types.ErrorMessageType;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    @Autowired
    private InfoApi infoApi;

    private final MessageUtils messageUtils;

    public CustomExceptionHandler(final MessageUtils messageUtils) {
        this.messageUtils = messageUtils;
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public final ResponseEntity<ErrorResponse> handleMissingRequestHeaderException(
            final MissingRequestHeaderException ex,
            final HttpServletRequest request) {
        return handleException(ex, request, ErrorMessageType.NULL_FIELD.getMessage(),
                ErrorMessageType.NULL_FIELD.getCode(),
                ErrorMessageType.NULL_FIELD.getHttpStatus());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public final ResponseEntity<ErrorResponse> handleHttpMessageNotReadableException(
            final HttpMessageNotReadableException ex,
            final HttpServletRequest request) {
        return handleException(ex, request, ErrorMessageType.BODY_MISSING.getMessage(),
                ErrorMessageType.BODY_MISSING.getCode(),
                ErrorMessageType.BODY_MISSING.getHttpStatus());
    }

    private ResponseEntity<ErrorResponse> handleException(final Exception ex, final HttpServletRequest request,
                                                          final String message, final String code,
                                                          final HttpStatus status) {

        final String path = (String) request.getAttribute("org.springframework.web.servlet.HandlerMapping"
                + ".pathWithinHandlerMapping");

        final String errorName = ex.getClass().getSimpleName();

        String newMessage = ex.getLocalizedMessage();

        if (errorName.contains("MissingRequestHeaderException")) {
            MissingRequestHeaderException missingRequestHeaderException = (MissingRequestHeaderException) ex;
            String headerName = missingRequestHeaderException.getHeaderName();
            newMessage = messageUtils.get(message, MessageUtils.getLocale(request), headerName);
        }

        if (errorName.contains("HttpMessageNotReadableException")) {
            newMessage = messageUtils.get(message, MessageUtils.getLocale(request));
        }

        ErrorResponse errorResponse = ErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(status.value())
                .traceId(request.getHeader("x-trace-id"))
                .code(code)
                .message(newMessage)
                .path(path)
                .cause(errorName)
                .build();

        log.error(errorResponse.toString());

        return new ResponseEntity<>(errorResponse, status);
    }

}
