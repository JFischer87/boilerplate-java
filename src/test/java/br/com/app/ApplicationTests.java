package br.com.app;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class ApplicationTest {

    @Test
    void contextLoads() {
        Properties properties = new Properties();
        properties.setProperty("webclient.client.connectTimeout", "1000");
        properties.setProperty("webclient.response.size", "2097152");
        properties.setProperty("restapi.client.connectTimeout", "1000");
        properties.setProperty("restapi.client.readTimeout", "1000");
        properties.setProperty("server.port", "9999");

        SpringApplication application = new SpringApplication(Application.class);
        application.setDefaultProperties(properties);
        assertDoesNotThrow(() -> {
            application.run();
        });
    }

    @Test
    void testMainMethodThrows() {
        assertThrows(BeanCreationException.class, () -> Application.main(new String[]{}));
    }
}
